const webpack = require("webpack");
const path = require('path');
// const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ExtractTextPlugin = require('mini-css-extract-plugin');

module.exports = function(env) {
    const isProduction = env === 'production';
    const cssExtract = new ExtractTextPlugin('styles.css');

    return {
        entry: './src/app.js',
        output: {
            path: path.join(__dirname, 'bin/'),
            filename: 'app.js'
        },
        mode: isProduction ? 'production' : "development",
        module: {
            rules: [{
                loader: 'babel-loader',
                test: /\.js$/,
                exclude: /node_modules/
            }, {
                test: /\.s?css$/,
                use: [ExtractTextPlugin.loader, 'css-loader', 'sass-loader'] 
                }
            ]
        },
        devServer: {
            contentBase: path.join(__dirname, 'bin/'),
            historyApiFallback: true,
        },
        plugins: [
            cssExtract
        ],
        devtool: "cheap-module-eval-source-map"
    }
}
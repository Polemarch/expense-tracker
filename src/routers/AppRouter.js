import { BrowserRouter, Route, Switch } from 'react-router-dom'
import React from 'react';
import { HelpPage } from '../components/HelpPage';
import EditExpensePage from '../components/EditExpensePage';
import { ExpenseDashboardPage } from '../components/ExpenseDashboardPage';
import Header from '../components/Header';
import  AddExpensePage  from '../components/AddExpensePage';
import { NotFoundPage } from '../components/NotFoundPage';
import LoginPage from '../components/LoginPage';
import PrivateRoute from '../components/PrivateRoute';

export const AppRouter = () => (
    <BrowserRouter>
        <div>
            <Switch>
                <PrivateRoute exact path="/" component={LoginPage} shouldBeLoggedIn={false} redirectPage="/dashboard"/>
                <PrivateRoute exact path="/dashboard" component={ExpenseDashboardPage} shouldBeLoggedIn={true} redirectPage="/"/>
                <PrivateRoute path="/create" component={AddExpensePage} shouldBeLoggedIn={true} redirectPage="/"/>
                <PrivateRoute path="/edit/:id" component={EditExpensePage} shouldBeLoggedIn={true} redirectPage="/"/>
                <PrivateRoute path="/help" component={HelpPage} shouldBeLoggedIn={true} redirectPage="/"/>

                <Route component={NotFoundPage} />
            </Switch>
        </div>
    </BrowserRouter>
);
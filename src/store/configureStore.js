import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { expensesReducer } from '../reducers/expensesReducer';
import { filtersReducer } from '../reducers/filtersReducer';
import thunk from 'redux-thunk';
import userReducer from '../reducers/userReducer';

const composeEnchancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
    const store = createStore(
        combineReducers({
            expenses: expensesReducer,
            filters: filtersReducer,
            user: userReducer
        }),
        composeEnchancers(applyMiddleware(thunk))
        // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );

    return store;
}


import React from 'react';
import ReactDOM from 'react-dom';

const Info = (props) => (
    <div>
        <h1>Info</h1>
        <p> Super secret info for: {props.title}</p>
    </div>
);

const Message = (props) => (
    <div>
        <p>{props.title} Not logged in</p>
    </div>
)

const authentificationWrapper = (WrappedComponent) => {
    return (props) => (
        <div>
            {
                props.loggedIn 
                ? <WrappedComponent {...props}/>
                : <Message {...props} />
            }
        </div>
    );
}

const ShownInfo = authentificationWrapper(Info);

ReactDOM.render(<ShownInfo loggedIn={true} title="Andrew"/>, document.getElementById('reactDiv'))

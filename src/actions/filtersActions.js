export const setTextFilter = (text = '') => ({
    type: "SET_TEXT_FILTER",
    text
})

export const setSortByAmount = () => ({
    type: 'SET_SORT',
    sortBy: 'amount'
})

export const setSortByDate = () => ({
    type: 'SET_SORT',
    sortBy: 'date'
})

export const setSortStartDate = (startDate) => ({
    type: 'SET_START_DATE',
    startDate
})

export const setSortEndDate = (endDate) => ({
    type: 'SET_END_DATE',
    endDate
})
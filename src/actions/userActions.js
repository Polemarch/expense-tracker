export const userLogIn = () => ({
    type: 'LOG_IN'
})

export const userLogOut = () => ({
    type: 'LOG_OUT'
})
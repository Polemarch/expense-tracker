import uuid from 'uuid';

export const addExpense = ( {description = '', note = '', amount = 0, createdAt = 0} = {}) => ({
    type: 'ADD_EXPENSE',
    expense: {
        id: uuid(),
        description,
        note,
        amount,
        createdAt
    }
})

export const startAddExpense = (expenseData = {}) => {
    return (dispatch) => {
        const {description = '', note = '', amount = 0, createdAt = 0} = expenseData;
        return new Promise((resolve) => {
            setTimeout(() => {
                dispatch(addExpense(expenseData))
                resolve();
            }, 500);
        })
    }
}

export const removeExpense = (id) => ({
    type: 'REMOVE_EXPENSE',
    id:id
})

export const editExpense = (id, updates) => ({
    type: 'EDIT_EXPENSE',
    id,
    updates
})
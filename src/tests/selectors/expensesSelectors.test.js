import { getVisibleExpenses, getExpensesTotal } from "../../selectors/expensesSelectors";
import moment from "moment";
import expenses from './../fixtures/expenses'

test('should filter by text value', () => {
    const filters = {
        text: 'e',
        sortBy: 'date',
        startDate: undefined,
        endDate: undefined
    }
    const result = getVisibleExpenses(expenses, filters);
    expect(result).toEqual([ expenses[2], expenses[1]])
})

test('should filter by start date', () => {
    const filters = {
        text: '',
        sortBy: 'date',
        startDate: moment(0),
        endDate: undefined
    }
    const result = getVisibleExpenses(expenses, filters);
    expect(result).toEqual([  expenses[2], expenses[0]])
})

test('should show total amount', () => {
    const result = getExpensesTotal(expenses);
    expect(result).toBe(495);
})

test('should show 0 amount', () => {
    const result = getExpensesTotal([]);
    expect(result).toBe(0);
})

test('should show amount of 1 item', () => {
    const result = getExpensesTotal([expenses[0]]);
    expect(result).toBe(195);
})
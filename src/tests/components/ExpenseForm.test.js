import React from 'react';
import { shallow } from 'enzyme';
import ExpenseForm from '../../components/ExpenseForm';
import expenses from './../fixtures/expenses';
import moment from 'moment';

test('test expense form', () => {
    const wrapper = shallow(<ExpenseForm/>);
    expect(wrapper).toMatchSnapshot();
});

test('should render ExpenseForm with expense data', () => {
    const wrapper = shallow(<ExpenseForm initialData={expenses[0]} />);
    expect(wrapper).toMatchSnapshot();
})

test('should render error for invalid form submition', () => {
    const wrapper = shallow(<ExpenseForm />);
    expect(wrapper).toMatchSnapshot();

    wrapper.find('form').simulate('submit', {preventDefault:()=> {}});

    expect(wrapper.state('error').length).toBeGreaterThan(0);
    expect(wrapper).toMatchSnapshot();
})

test('should set description on input change', () => {
    const wrapper = shallow(<ExpenseForm/>);
    wrapper.find('input').at(0).simulate('change', {
        target: {value: 'some description'}
    });
    expect(wrapper.state('description')).toBe('some description')
})

test('should set note on textarea change', () => {
    const value = 'some description'
    const wrapper = shallow(<ExpenseForm/>);
    wrapper.find('textarea').at(0).simulate('change', {
        target: {value}
    });
    expect(wrapper.state('note')).toBe(value)
})

test('should set amount', () => {
    const value = '122.22'
    const wrapper = shallow(<ExpenseForm/>);
    wrapper.find('input').at(1).simulate('change', {
        target: {value}
    });
    expect(wrapper.state('amount')).toBe(value)
})

test('should set invalid amount', () => {
    const value = '12a2.222'
    const wrapper = shallow(<ExpenseForm/>);
    wrapper.find('input').at(1).simulate('change', {
        target: {value}
    });
    expect(wrapper.state('amount')).toBe('')
})

test('should call onSubmitForm prop for valid form submission', () => {
    const onSubmitSpy = jest.fn();
    const wrapper = shallow(<ExpenseForm initialData={expenses[2]} onSubmit={onSubmitSpy}/>)
    wrapper.find('form').at(0).simulate('submit', { preventDefault: () => {}})

    expect(wrapper.state('error')).toBe(undefined);
    const {amount, createdAt, description, note} = expenses[2]
    expect(onSubmitSpy).toHaveBeenLastCalledWith({amount, createdAt, description, note})
})

test('should set new date on date change', () => {
    const now = moment()
    const wrapper = shallow(<ExpenseForm />);

    wrapper.find('withStyles(SingleDatePicker)').prop('onDateChange')(moment(now));
    expect(wrapper.state('createdAt')).toEqual(now);
})

test('should set calendar focus on change', () => {
    const focused = Math.random() > 0.5;

    const wrapper = shallow(<ExpenseForm />);
    wrapper.find('withStyles(SingleDatePicker)').prop('onFocusChange')({focused});
    expect(wrapper.state('calendarFocused')).toBe(focused);
})
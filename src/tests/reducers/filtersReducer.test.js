import { filtersReducer } from "../../reducers/filtersReducer"
import moment from "moment";


test('init reducer', () => {
    const state = filtersReducer(undefined, {type:"@@INIT"});
    expect(state).toEqual({
        text: '',
        sortBy: 'date',
        startDate: moment().startOf('month'),
        endDate: moment().endOf('month')
    })
})

test('set sort by amount', () => {
    const state = filtersReducer(undefined, {
        type: "SET_SORT",
        sortBy: 'amount'
    })
    expect(state.sortBy).toBe('amount')
})

test('set sort by date', () => {
    const initialReducer = {
        text: '',
        startDate: undefined,
        endDate: undefined,
        sortBy: "amount"
    }
    const state = filtersReducer(initialReducer, {
        type: "SET_SORT",
        sortBy: 'date'
    })
    expect(state.sortBy).toBe('date')
})
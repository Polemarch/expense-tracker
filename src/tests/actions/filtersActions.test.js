import { setTextFilter, setSortByAmount, setSortStartDate } from "../../actions/filtersActions"
import moment from "moment"

test('settings text filter', () => {
    expect(setTextFilter('test')).toEqual({
        type: "SET_TEXT_FILTER",
        text: 'test'
    })
})

test('set text filter without params', () => {
    expect(setTextFilter()).toEqual({
        type: "SET_TEXT_FILTER",
        text: ''
    })
})

test('set sort by amount', () => {
    expect(setSortByAmount()).toEqual({
        type: 'SET_SORT',
        sortBy: 'amount'
    })
})

test('set start date', () => {
    expect(setSortStartDate(moment(0))).toEqual({
        type: 'SET_START_DATE',
        startDate: moment(0)
    })
})
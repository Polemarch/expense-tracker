import { addExpense, removeExpense, editExpense, startAddExpense } from "../../actions/expensesActions"
import expenses from './../fixtures/expenses'
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

const createMockStore = configureMockStore([thunk]);

test('should setup remove expense action object', () => {
    const action = removeExpense('123abc');
    expect(action).toEqual({
        type: "REMOVE_EXPENSE",
        id: "123abc"
    })
})

test('async add expense', () => {
    const store = createMockStore({});

    const {description = '', note = '', amount = 0, createdAt = 0} = expenses[0];
    const expenseData = {description, note, amount, createdAt};

    return store.dispatch(startAddExpense(expenseData)).then(() => {
        const actions = store.getActions();
        expect(actions[0]).toEqual({
            type: "ADD_EXPENSE",
            expense: {
                id: expect.any(String),
                ...expenseData
            }
        })
    })
})

test('check edit expense', () => {
    const action = editExpense('123abc', {'note': 'SomeValue'});
    expect(action).toEqual({
        type: "EDIT_EXPENSE",
        id: "123abc",
        updates: {'note': 'SomeValue'}
    })
})

test('check add expense with fields', () => {
    const expenseData = {'note': 'SomeValue', amount:100, description:'rent', createdAt:1000}
    const action = addExpense(expenseData);
    expect(action).toEqual({
        type: "ADD_EXPENSE",
        expense: {
            id:expect.any(String) ,
             ...expenseData
        }
    })
})

test('check add expense without fields', () => {
    const expenseData = {'note': '', amount:0, description:'', createdAt:0}
    const action = addExpense();
    expect(action).toEqual({
        type: "ADD_EXPENSE",
        expense: {
            id:expect.any(String) ,
             ...expenseData
        }
    })
})
import React from 'react';
import { connect } from 'react-redux';
import { getVisibleExpenses, getExpensesTotal } from '../selectors/expensesSelectors';
import { Link } from 'react-router-dom';

export const ExpenseSummary = (props) => (
    <div className="page-header">
        <div className="content-container">
            {
                !props.expenses || props.expenses.length === 0 ? (
                    <p>No Expenses</p>
                ) : (
                    <p className="page-header__title">You have <span>{props.expenses.length}</span> expenses on total amount of: <span>${getExpensesTotal(props.expenses)}</span></p>
                )
            }
            <div className="page-header__actions">
                <Link className="button" to='/create'>Add expense</Link>
            </div>
        </div>
    </div>
)

const mapStateToProps = ({expenses, filters}) => {
    return {
        expenses: getVisibleExpenses(expenses, filters)
    }
}

export default connect(mapStateToProps)(ExpenseSummary);
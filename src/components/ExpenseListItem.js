import React from 'react'
import moment from 'moment';
import { Link } from 'react-router-dom';

const ExpenseListItem = (props) => (
    <div>
        <Link className="list-item" to={`/edit/${props.id}`}>
        <div>
            <h3 className="list-item__title">{props.description}</h3>
            <span className="list-item__subtitle">{moment(props.createdAt).format('MMMM Do, YYYY')}</span>
        </div>
        <h3 className="list-item__data">${props.amount}</h3>
           
        </Link>
    </div>
)

export default ExpenseListItem;
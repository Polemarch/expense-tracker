import { NavLink, Link } from 'react-router-dom'
import React from 'react';
import { userLogOut } from '../actions/userActions';
import { connect } from 'react-redux';

const Header = (props) => (
    <header className="header">
        <div className="content-container">
            <div className="header__content">
                <Link className="header__title" exact to="/">
                    <h1>Expesify</h1>
                </Link>

                {/* <NavLink to="/create" activeClassName="is-active">Create page</NavLink>
                <NavLink to="/help" activeClassName="is-active">Help page</NavLink> */}
                <button className="button button__link" onClick={props.logOut}>Log out</button>
            </div>
        </div>
    </header>
);

const mapDispatchToProps = (dispatch) => ({
    logOut: () => dispatch(userLogOut())
})

export default connect(undefined, mapDispatchToProps)(Header);
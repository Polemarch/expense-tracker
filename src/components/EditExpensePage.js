import React from 'react';
import { editExpense, removeExpense } from '../actions/expensesActions';
import { connect } from 'react-redux';
import ExpenseForm from './ExpenseForm';

const EditExpensePage = (props) => (
    <div>
        <div className="page-header">
            <div className="content-container">
                <h1 className="page-header__title">Edit expense</h1>
            </div>
        </div>

        <div className="content-container">
            <ExpenseForm 
                onSubmit={(expense) => {
                    props.dispatch(editExpense(props.match.params.id, expense));
                    props.history.push('/')
                }}
                initialData={props.expense}
            />
            <button className="button button__secondary" onClick={() => {
                props.dispatch(removeExpense(props.match.params.id));
                props.history.push('/')
            }}> Remove Expense</button>
        </div>
    </div>
);

const mapDispatchToProps = (state, props) => {
    return {
        expense: state.expenses.find((expense) => props.match.params.id === expense.id)
    }
}

export default connect(mapDispatchToProps)(EditExpensePage);
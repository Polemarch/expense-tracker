import React from 'react';
import { connect } from "react-redux"
import { Route, Redirect } from "react-router-dom"
import Header from './Header';

export const PrivateRoute = ({
    isLoggedIn,
    shouldBeLoggedIn,
    redirectPage,
    component: Component,
    ...rest
}) => (
    <div>
        {isLoggedIn && <Header/>}
        <Route {...rest} component={(props) => (
            (isLoggedIn === shouldBeLoggedIn) ? (
                <Component {...props}/>
            ) : (
                <Redirect to={redirectPage} />
            )
        )} />
    </div>
)

const mapStateToProps = (state) => ({
    isLoggedIn: state.user.loggedIn
})

export default connect(mapStateToProps)(PrivateRoute);
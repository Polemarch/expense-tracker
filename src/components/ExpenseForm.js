import React from 'react';
import moment from 'moment';
import { SingleDatePicker } from 'react-dates';


export default class ExpenseForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            description: props.initialData ?  props.initialData.description : '',
            note:  props.initialData ?  props.initialData.note : '',
            amount:  props.initialData ?  props.initialData.amount.toString() : '',
            createdAt: props.initialData ?  moment(props.initialData.createdAt) : moment(),
            calendarFocused: false,
            error: undefined
        }
    }
    
    onDescriptionChange = (e) => {
        const description = e.target.value;
        this.setState(() => ({description}));
    }

    onNoteChange = (e) => {
        const note = e.target.value;
        this.setState(() => ({note}));
    }

    onAmountChange = (e) => {
        const amount = e.target.value;
        if (!amount || amount.match(/^\d{1,}(\.\d{0,2})?$/)) {
            this.setState(() => ({amount}))
        }
    }

    onDateChange = (createdAt) => {
        createdAt && this.setState(() => ({createdAt}));
    }

    onCalendarFocusChange = ({ focused }) => {
        this.setState(() => ({calendarFocused: focused}))
    }

    onSubmit = (e) => {
        e.preventDefault();

        if (!this.state.description || !this.state.amount) {
            this.setState(() => ({error: 'Please provide description and amount'}));
        } else {
            this.setState(() => ({error: undefined}));
            this.props.onSubmit({
                description:this.state.description,
                amount: Number.parseFloat(this.state.amount, 10),
                createdAt: this.state.createdAt.valueOf(),
                note: this.state.note
            })
        }
    } 

    render() {
        return (
            <form className="form" onSubmit={this.onSubmit}>
                <input
                    type="text"
                    placeholder="Description"
                    autoFocus
                    className="text-input"
                    value={this.state.description}
                    onChange={this.onDescriptionChange}
                />
                <input
                    type="text"
                    placeholder="amount"
                    className="text-input"
                    value={this.state.amount}
                    onChange={this.onAmountChange}
                />
                <SingleDatePicker 
                    date={this.state.createdAt}
                    onDateChange={this.onDateChange}
                    focused={this.state.calendarFocused}
                    onFocusChange={this.onCalendarFocusChange}
                    numberOfMonths={1}
                    isOutsideRange={() => false}
                />
                <textarea 
                    placeholder="Add a note (optional)"
                    value={this.state.note}
                    className="textarea"
                    onChange={this.onNoteChange}
                >
                </textarea>

                {this.state.error && <p className="form__error">{this.state.error}</p>}
                <div>
                    <button className="button">Save expense</button>
                </div>
            </form>
        )
    }
}
import React from 'react';
import { connect } from 'react-redux';
import { userLogIn } from '../actions/userActions';

class LoginPage extends React.Component {
    render() {
        return (
            <div className="box-layout">
                <div className="box-layout__box">
                    <h1 className="box-layout__title">Expensify app</h1>
                    <p>It's time to get expenses under control</p>
                    <button className="button" onClick={this.props.startLogin}>Login</button>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    startLogin: () => dispatch(userLogIn())
})

export default connect(undefined, mapDispatchToProps)(LoginPage);
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { AppRouter } from './routers/AppRouter'
import configureStore from './store/configureStore';
import { addExpense } from './actions/expensesActions';
import { LoadingPage } from './components/LoadingPage';
 
import moment from 'moment';

import './styles/style.scss'; 
import 'react-dates/lib/css/_datepicker.css'
import 'normalize.css/normalize.css'
import 'react-dates/initialize';

const store = configureStore();


const jsx = (
    <Provider store={store}>
        <AppRouter />
    </Provider>

    // <LoadingPage />
);
;
store.dispatch(addExpense({description:'Some', amount:"100", createdAt:moment()}))
store.dispatch(addExpense({description:'Two', amount:"20", createdAt:moment()}))

ReactDOM.render(jsx, document.getElementById("reactDiv"));